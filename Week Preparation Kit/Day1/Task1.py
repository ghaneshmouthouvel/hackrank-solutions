#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'plusMinus' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#

def plusMinus(arr):
    count = {"positive":0, 'negative':0, "zero": 0}
    for x in arr:
        if x > 0:
            count["positive"] += 1
        if x < 0:
            count["negative"] += 1
        if x == 0:
            count["zero"] += 1
    return [count["positive"]/len(arr), count["negative"]/len(arr), count["zero"]/len(arr)]

if __name__ == '__main__':
    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    results = plusMinus(arr)
    for result in results:
        print(result)