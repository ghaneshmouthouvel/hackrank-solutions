#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'miniMaxSum' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#

def miniMaxSum(arr):
    val = sorted(arr)
    return [sum(val[:4]), sum(val[-4:])]

if __name__ == '__main__':

    arr = list(map(int, input().rstrip().split()))

    results = miniMaxSum(arr)
    print(results[0],results[1])